/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.datalab2;

/**
 *
 * @author Paweena Chinasri
 */
public class ArrayDeletionsLab {

    public static void main(String[] args) {
        int[] testArray = {1, 2, 3, 4, 5};
        
        System.out.println("Original Array: " + arrayToString(testArray));
        
        int[] updatedArray1 = deleteElementByIndex(testArray, 2);
        System.out.println("Array after deleting element at index 2: " + arrayToString(updatedArray1));
        
        int[] updatedArray2 = deleteElementByValue(testArray, 4);
        System.out.println("Array after deleting element with value 4: " + arrayToString(updatedArray2));
    }

    public static int[] deleteElementByIndex(int[] arr, int index) {
        if (index < 0 || index >= arr.length) {
            System.out.println("Invalid index");
            return arr;
        }

        int[] updatedArray = new int[arr.length - 1];
        for (int i = 0, j = 0; i < arr.length; i++) {
            if (i == index) {
                continue; // Skip the element at the specified index
            }
            updatedArray[j++] = arr[i];
        }
        return updatedArray;
    }

    public static int[] deleteElementByValue(int[] arr, int value) {
        int indexToDelete = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                indexToDelete = i;
                break;
            }
        }

        if (indexToDelete == -1) {
            System.out.println("Value not found in array");
            return arr;
        }

        return deleteElementByIndex(arr, indexToDelete);
    }

    public static String arrayToString(int[] arr) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < arr.length; i++) {
            sb.append(arr[i]);
            if (i < arr.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
 }

